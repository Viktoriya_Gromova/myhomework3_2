//
//  main.m
//  homework3(2)
//
//  Created by Victoria on 15.11.15.
//  Copyright © 2015 ViktoriyaGromova. All rights reserved.
//

#import <Foundation/Foundation.h>

void numberCycle();
bool checkMultiplicity();


int main(int argc, const char * argv[]) {
    @autoreleasepool {
        numberCycle(99, 3, 5);
    }
    return 0;
}

void numberCycle(int maxNumber, int number, int multiplicity)
{
    
    for(int a=maxNumber; a<=maxNumber; a-=number)
    {
        NSLog(@"%i", a);
        
        if(a<=0)
        {
            break;
        }
        
        if(checkMultiplicity(a, multiplicity))
        {
            NSLog(@"Found one!");
        }
    }
}

bool checkMultiplicity(int a, int multiplicity)
{
    bool result = (a % multiplicity==0);
    
    if(result)
        result = true;
    
    else
        result = false;
    
    return result;
}
